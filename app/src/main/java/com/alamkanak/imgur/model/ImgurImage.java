package com.alamkanak.imgur.model;

import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Alam on 4/10/2017.
 */
public class ImgurImage {

    @Expose
    private String id;

    @Expose
    private String title;

    @Expose
    private String description;

    @Expose
    private String cover;

    @SerializedName("cover_width")
    @Expose
    private Integer coverWidth;

    @SerializedName("cover_height")
    @Expose
    private Integer coverHeight;

    @Expose
    private String layout;

    @Expose
    private String link;

    @Expose
    private Integer ups;

    @Expose
    private Integer downs;

    @Expose
    private Integer points;

    @Expose
    private Integer score;

    @SerializedName("is_album")
    @Expose
    private Boolean isAlbum;

    @Expose
    private Integer width;

    @Expose
    private Integer height;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public @Nullable String getDescription() {
        return description;
    }

    public void setDescription(@Nullable String description) {
        this.description = description;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public Integer getCoverWidth() {
        return coverWidth;
    }

    public void setCoverWidth(Integer coverWidth) {
        this.coverWidth = coverWidth;
    }

    public Integer getCoverHeight() {
        return coverHeight;
    }

    public void setCoverHeight(Integer coverHeight) {
        this.coverHeight = coverHeight;
    }

    public String getLayout() {
        return layout;
    }

    public void setLayout(String layout) {
        this.layout = layout;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Integer getUps() {
        return ups;
    }

    public void setUps(Integer ups) {
        this.ups = ups;
    }

    public Integer getDowns() {
        return downs;
    }

    public void setDowns(Integer downs) {
        this.downs = downs;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Boolean isAlbum() {
        return isAlbum;
    }

    public void setIsAlbum(Boolean isAlbum) {
        this.isAlbum = isAlbum;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public String getImageUrl() {
        return isAlbum ? "http://i.imgur.com/" + getCover() + ".png" : getLink();
    }

    public int getWidth() {
        if (isAlbum())
            return getCoverWidth();
        else return width;
    }

    public int getHeight() {
        if (isAlbum())
            return getCoverHeight();
        else return height;
    }
}
