package com.alamkanak.imgur.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Alam on 4/10/2017.
 */

public class Gallery {

    @Expose @SerializedName("data")
    List<ImgurImage> images;

    public List<ImgurImage> getImages() {
        return images;
    }

    public void setImages(List<ImgurImage> images) {
        this.images = images;
    }
}
