package com.alamkanak.imgur;

import android.app.Application;
import android.util.Log;

import timber.log.Timber;

/**
 * Created by Alam on 4/10/2017.
 */

public class ImgurApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.plant(new Timber.DebugTree());
    }

}
