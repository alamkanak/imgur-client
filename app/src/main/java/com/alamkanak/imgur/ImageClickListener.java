package com.alamkanak.imgur;

import com.alamkanak.imgur.model.ImgurImage;

/**
 * Created by Alam on 4/11/2017.
 */

public interface ImageClickListener {

    void onImageClick(ImgurImage imgurImage);

}
