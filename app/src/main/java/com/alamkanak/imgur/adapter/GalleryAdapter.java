package com.alamkanak.imgur.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.alamkanak.imgur.ImageClickListener;
import com.alamkanak.imgur.R;
import com.alamkanak.imgur.model.ImgurImage;
import com.alamkanak.imgur.view.SquareImageView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alam on 4/10/2017.
 */
public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    public static final int TYPE_GRID = 1;
    public static final int TYPE_STAGGERED_GRID = 2;
    private final Context context;
    private final ImageClickListener listener;
    private List<ImgurImage> images;
    private int layoutType = TYPE_GRID;

    static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_view_photo)
        SquareImageView imageViewPhoto;
        @BindView(R.id.text_view_title)
        TextView textViewTitle;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public GalleryAdapter(Context context, List<ImgurImage> images, ImageClickListener listener) {
        this.images = images;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_imgur_image, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.textViewTitle.setText(images.get(position).getTitle());
        if (layoutType == TYPE_STAGGERED_GRID) {
            holder.imageViewPhoto.setSquare(false);
            holder.imageViewPhoto.setSize(images.get(position).getWidth(), images.get(position).getHeight());
        }
        else {
            holder.imageViewPhoto.setSquare(true);
        }

        // Show image from local cache first. Then download the image from network.
        Picasso.with(context)
                .load(images.get(position).getImageUrl())
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(holder.imageViewPhoto, new Callback() {
                    @Override
                    public void onSuccess() {

                    }

                    @Override
                    public void onError() {
                        int index = holder.getAdapterPosition();
                        if (index < 0)
                            return;
                        Picasso.with(context)
                                .load(images.get(index).getImageUrl())
                                .into(holder.imageViewPhoto);
                    }
                });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onImageClick(images.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public void setData(List<ImgurImage> images) {
        this.images = images;
    }

    public void setLayoutType(int layoutType) {
        this.layoutType = layoutType;
    }

    public void clear() {
        images.clear();
        notifyDataSetChanged();
    }
}
