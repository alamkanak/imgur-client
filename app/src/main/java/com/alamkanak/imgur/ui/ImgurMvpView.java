package com.alamkanak.imgur.ui;

import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Created by Alam on 4/10/2017.
 */

public interface ImgurMvpView<M> extends MvpView {

    void showData(M data);
    void showError();
    void showLoading();

}
