package com.alamkanak.imgur.ui;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.MenuRes;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.alamkanak.imgur.presenter.GalleryPresenter;
import com.alamkanak.imgur.ImageClickListener;
import com.alamkanak.imgur.ImgurSettings;
import com.alamkanak.imgur.R;
import com.alamkanak.imgur.adapter.GalleryAdapter;
import com.alamkanak.imgur.model.ImgurImage;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.alamkanak.imgur.R.string.about;

public class GalleryActivity extends MvpActivity<ImgurMvpView<List<ImgurImage>>, GalleryPresenter> implements ImgurMvpView<List<ImgurImage>>, ImageClickListener {

    private static final String TAG_RETAIN_FRAGMENT = "retain";
    @BindView(R.id.recycler_view_images)
    RecyclerView mRecyclerView;
    @BindView(R.id.linear_layout_error)
    LinearLayout mLinearLayoutError;
    @BindView(R.id.linear_layout_loading)
    LinearLayout mLinearLayoutLoading;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.coordinator_layout_root)
    View rootLayout;

    private GalleryAdapter mAdapter;
    private PopupMenu mPopup;
    private TextView mTextViewSection;
    private TextView mTextViewSort;
    private TextView mTextViewWindow;
    private CheckBox mCheckBoxShowViral;
    private RetainFragment mRetainFragment;
    private boolean mRetainedFragment = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        // Setup retain fragment for orientation change.
        FragmentManager fm = getSupportFragmentManager();
        mRetainFragment = (RetainFragment) fm.findFragmentByTag(TAG_RETAIN_FRAGMENT);
        if (mRetainFragment == null) {
            mRetainFragment = new RetainFragment();
            fm.beginTransaction().add(mRetainFragment, TAG_RETAIN_FRAGMENT).commit();
        } else if (mRetainFragment.getImages() != null && mRetainFragment.getImages().size() > 0) {
            mRetainedFragment = true;
        }

        // Setup recycler view.
        DividerItemDecoration verticalDivider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        DividerItemDecoration horizontalDivider = new DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL);
        verticalDivider.setDrawable(getDrawable(R.drawable.divider));
        horizontalDivider.setDrawable(getDrawable(R.drawable.divider));
        mRecyclerView.addItemDecoration(verticalDivider);
        mRecyclerView.addItemDecoration(horizontalDivider);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new GalleryAdapter(this, new ArrayList<ImgurImage>(), this);
        mRecyclerView.setAdapter(mAdapter);
        refreshRecyclerViewLayout(!isRecyclerViewStaggered(false));

        if (mRetainedFragment) {
            showData(mRetainFragment.getImages());
        } else {
            getPresenter().fetchImages(true);
        }
    }

    @NonNull
    @Override
    public GalleryPresenter createPresenter() {
        return new GalleryPresenter();
    }

    @Override
    public void showData(List<ImgurImage> images) {
        mRetainFragment.setImages(images);
        mAdapter.setData(images);
        mAdapter.notifyDataSetChanged();
        mRecyclerView.setVisibility(View.VISIBLE);
        mLinearLayoutError.setVisibility(View.GONE);
        mLinearLayoutLoading.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        if (mAdapter.getItemCount() > 0) {
            Snackbar.make(rootLayout, R.string.error, Snackbar.LENGTH_LONG)
                    .show();
        } else {
            mRecyclerView.setVisibility(View.GONE);
            mLinearLayoutError.setVisibility(View.VISIBLE);
            mLinearLayoutLoading.setVisibility(View.GONE);
        }
    }

    @Override
    public void showLoading() {
        mRecyclerView.setVisibility(View.GONE);
        mLinearLayoutError.setVisibility(View.GONE);
        mLinearLayoutLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onImageClick(ImgurImage imgurImage) {
        ImageActivity.startActivity(this, imgurImage);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gallery, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean isStaggered = !isRecyclerViewStaggered(false);
        MenuItem item = menu.findItem(R.id.menu_item_list_type);
        item.setIcon(isStaggered ? R.drawable.ic_staggered_grid : R.drawable.ic_grid);
        item.setTitle(isStaggered ? R.string.menu_title_grid : R.string.menu_title_staggered);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_filter:
                MaterialDialog dialog = new MaterialDialog.Builder(this)
                        .title(R.string.filter)
                        .customView(R.layout.dialog_filter, true)
                        .positiveText(R.string.apply)
                        .negativeText(R.string.cancel)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                saveValues();
                                mAdapter.clear();
                                getPresenter().fetchImages(false);
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
                View view = dialog.getCustomView();
                if (view == null)
                    break;

                // Get views.
                LinearLayout linearLayoutSection = (LinearLayout) view.findViewById(R.id.linear_layout_section);
                LinearLayout linearLayoutSort = (LinearLayout) view.findViewById(R.id.linear_layout_sort);
                LinearLayout linearLayoutWindow = (LinearLayout) view.findViewById(R.id.linear_layout_window);
                LinearLayout linearLayoutShowViral = (LinearLayout) view.findViewById(R.id.linear_layout_show_viral);
                mTextViewSection = (TextView) view.findViewById(R.id.text_view_section);
                mTextViewSort = (TextView) view.findViewById(R.id.text_view_sort);
                mTextViewWindow = (TextView) view.findViewById(R.id.text_view_window);
                mCheckBoxShowViral = (CheckBox) view.findViewById(R.id.check_box_show_viral);

                // Populate the dialog.
                refreshDialog(mTextViewSection, mTextViewSort, mTextViewWindow, mCheckBoxShowViral);

                // Add click setters.
                linearLayoutSection.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showPopupMenu(mTextViewSection, R.menu.section);
                    }
                });
                linearLayoutSort.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showPopupMenu(mTextViewSort, R.menu.sort);
                    }
                });
                linearLayoutWindow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showPopupMenu(mTextViewWindow, R.menu.window);
                    }
                });
                linearLayoutShowViral.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mCheckBoxShowViral.setChecked(!mCheckBoxShowViral.isChecked());
                    }
                });

                break;
            case R.id.menu_item_list_type:
                boolean isStaggered = isRecyclerViewStaggered(true);
                refreshRecyclerViewLayout(isStaggered);
                invalidateOptionsMenu();
                break;
            case R.id.menu_item_about:
                MaterialDialog aboutDialog = new MaterialDialog.Builder(this)
                        .title(about)
                        .customView(R.layout.dialog_about, true)
                        .positiveText(R.string.ok)
                        .show();
                View aboutView = aboutDialog.getCustomView();
                ((TextView) aboutView.findViewById(R.id.text_view_app_version)).setText(getAppVersion());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Get app version.
     * @return The app version.
     */
    private String getAppVersion() {
        try {
            PackageManager manager = this.getPackageManager();
            PackageInfo info = manager.getPackageInfo(this.getPackageName(), 0);
            return info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "1.0";
    }

    @OnClick(R.id.button_retry)
    public void onRetry() {
        getPresenter().fetchImages(false);
    }

    /**
     * Get whether user chose the recycler view to be a staggered grid view.
     * @param switchValue True if the value should be swtiched and saved.
     * @return whether user chose the recycler view to be a staggered grid view.
     */
    private boolean isRecyclerViewStaggered(boolean switchValue) {
        ImgurSettings settings = new ImgurSettings(this);
        boolean isStaggered = settings.getBoolean(ImgurSettings.KEY_STAGGERED, true);
        if (switchValue)
            settings.write(ImgurSettings.KEY_STAGGERED, !isStaggered);
        return isStaggered;
    }

    /**
     * Change the layout of the recycler view to be staggered or not staggered.
     * @param isStaggered True if the recycler view should be rendered staggered.
     */
    private void refreshRecyclerViewLayout(boolean isStaggered) {
        mRecyclerView.setLayoutManager(
                isStaggered ?
                        new GridLayoutManager(this, getResources().getInteger(R.integer.grid_span_count)) :
                        new StaggeredGridLayoutManager(getResources().getInteger(R.integer.staggered_grid_span_count), StaggeredGridLayoutManager.VERTICAL)
        );
        mAdapter.setLayoutType(
                isStaggered ?
                        GalleryAdapter.TYPE_GRID :
                        GalleryAdapter.TYPE_STAGGERED_GRID);
        mAdapter.notifyDataSetChanged();
        mRecyclerView.invalidate();
    }

    /**
     * Save user settings for filters.
     */
    private void saveValues() {
        ImgurSettings settings = new ImgurSettings(this);
        settings.write(ImgurSettings.KEY_SECTION, mTextViewSection.getText().toString());
        settings.write(ImgurSettings.KEY_SORT, mTextViewSort.getText().toString());
        settings.write(ImgurSettings.KEY_WINDOW, mTextViewWindow.getText().toString());
        settings.write(ImgurSettings.KEY_SHOW_VIRAL, mCheckBoxShowViral.isChecked());
        settings.write(ImgurSettings.KEY_IMGUR_RESPONSE, "");
    }

    /**
     * Show correct values in the filter dialog.
     * @param textViewSection The text view to show selected section name.
     * @param textViewSort The text view to show selected sort name.
     * @param textViewWindow The text view to show selected window name.
     * @param checkBoxShowViral The checkbox that should reflect the viral option.
     */
    private void refreshDialog(TextView textViewSection, TextView textViewSort, TextView textViewWindow, CheckBox checkBoxShowViral) {
        ImgurSettings settings = new ImgurSettings(this);
        textViewSection.setText(settings.getString(ImgurSettings.KEY_SECTION, getString(R.string.section_hot)));
        textViewSort.setText(settings.getString(ImgurSettings.KEY_SORT, getString(R.string.sort_viral)));
        textViewWindow.setText(settings.getString(ImgurSettings.KEY_WINDOW, getString(R.string.window_day)));
        checkBoxShowViral.setChecked(settings.getBoolean(ImgurSettings.KEY_SHOW_VIRAL, true));
    }

    /**
     * Show popup menu in the filter dialog below a specific text view.
     * @param textView The textview under which the popup menu should be displayed.
     * @param section The menu to be inflated under the textview.
     */
    private void showPopupMenu(final TextView textView, @MenuRes int section) {
        if (mPopup != null)
            mPopup.dismiss();
        mPopup = new PopupMenu(GalleryActivity.this, textView);
        mPopup.setGravity(Gravity.RIGHT);
        mPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                textView.setText(item.getTitle());
                return true;
            }
        });
        MenuInflater inflater = mPopup.getMenuInflater();
        inflater.inflate(section, mPopup.getMenu());
        mPopup.show();
    }
}
