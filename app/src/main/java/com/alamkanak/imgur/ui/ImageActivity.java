package com.alamkanak.imgur.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.alamkanak.imgur.presenter.ImagePresenter;
import com.alamkanak.imgur.R;
import com.alamkanak.imgur.model.ImgurImage;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageActivity extends MvpActivity<ImgurMvpView<ImgurImage>, ImagePresenter> implements ImgurMvpView<ImgurImage> {

    private static final String EXTRA_TITLE = "title";
    private static final String EXTRA_DESCRIPTION = "description";
    private static final String EXTRA_URL = "url";
    private static final String EXTRA_UP_VOTES = "up_votes";
    private static final String EXTRA_DOWN_VOTES = "down_votes";
    private static final String EXTRA_SCORE = "score";

    @BindView(R.id.text_view_title)
    TextView mTextViewTitle;
    @BindView(R.id.text_view_description)
    TextView mTextViewDescription;
    @BindView(R.id.text_view_upvotes)
    TextView mTextViewUpVotes;
    @BindView(R.id.text_view_downvotes)
    TextView mTextViewDownVotes;
    @BindView(R.id.text_view_scores)
    TextView mTextViewScore;
    @BindView(R.id.image_view_photo)
    ImageView mImageViewPhoto;
    @BindView(R.id.view_separator)
    View mViewSeparator;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Setup activity.
        setContentView(R.layout.activity_image);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        android.support.v7.app.ActionBar toolbar = getSupportActionBar();
        if (toolbar != null)
            toolbar.setDisplayHomeAsUpEnabled(true);

        // Get extras.
        ImgurImage imgurImage = new ImgurImage();
        imgurImage.setTitle(getIntent().getStringExtra(EXTRA_TITLE));
        imgurImage.setDescription(getIntent().getStringExtra(EXTRA_DESCRIPTION));
        imgurImage.setLink(getIntent().getStringExtra(EXTRA_URL));
        imgurImage.setUps(getIntent().getIntExtra(EXTRA_UP_VOTES, 0));
        imgurImage.setDowns(getIntent().getIntExtra(EXTRA_DOWN_VOTES, 0));
        imgurImage.setScore(getIntent().getIntExtra(EXTRA_SCORE, 0));
        getPresenter().setImage(imgurImage);
    }

    @NonNull
    @Override
    public ImagePresenter createPresenter() {
        return new ImagePresenter();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Start ImageActivity with given parameters.
     * @param context The context who is starting ImageActivity.
     * @param imgurImage The image to be shown in ImageActivity.
     */
    public static void startActivity(Context context, ImgurImage imgurImage) {
        Intent intent = new Intent(context, ImageActivity.class);
        intent.putExtra(EXTRA_TITLE, imgurImage.getTitle());
        intent.putExtra(EXTRA_DESCRIPTION, imgurImage.getDescription());
        intent.putExtra(EXTRA_URL, imgurImage.getImageUrl());
        intent.putExtra(EXTRA_UP_VOTES, imgurImage.getUps());
        intent.putExtra(EXTRA_DOWN_VOTES, imgurImage.getDowns());
        intent.putExtra(EXTRA_SCORE, imgurImage.getScore());
        context.startActivity(intent);
    }

    @Override
    public void showData(ImgurImage imgurImage) {
        Picasso.with(this)
                .load(imgurImage.getLink())
                .into(mImageViewPhoto);
        if (!TextUtils.isEmpty(imgurImage.getTitle())) {
            setTitle(imgurImage.getTitle());
        }
        showText(mTextViewTitle, imgurImage.getTitle());
        showText(mTextViewDescription, imgurImage.getDescription());
        if (TextUtils.isEmpty(imgurImage.getTitle()) && TextUtils.isEmpty(imgurImage.getDescription())) {
            mViewSeparator.setVisibility(View.GONE);
        }
        mTextViewUpVotes.setText(String.valueOf(imgurImage.getUps()));
        mTextViewDownVotes.setText(String.valueOf(imgurImage.getDowns()));
        mTextViewScore.setText(String.valueOf(imgurImage.getScore()));
    }

    /**
     * Show text in a text view or hide it if the text is null.
     * @param textView The text view.
     * @param text The text to be shown.
     */
    private static void showText(TextView textView, @Nullable String text) {
        if (TextUtils.isEmpty(text)) {
            textView.setVisibility(View.GONE);
        }
        else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(text);
        }
    }

    @Override
    public void showError() {

    }

    @Override
    public void showLoading() {

    }
}
