package com.alamkanak.imgur.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.alamkanak.imgur.model.ImgurImage;

import java.util.List;

/**
 * A fragment that should retain gallery images for orientation change.
 * Created by Alam on 4/12/2017.
 */
public class RetainFragment extends Fragment {

    private List<ImgurImage> mImages;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public List<ImgurImage> getImages() {
        return mImages;
    }

    public void setImages(List<ImgurImage> images) {
        this.mImages = images;
    }
}
