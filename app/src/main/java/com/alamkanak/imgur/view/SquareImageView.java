package com.alamkanak.imgur.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

import timber.log.Timber;

/**
 * An image view that can either be square in shape or it can adjust it size to fit in the
 * staggered grid view.
 *
 * Created by Alam on 4/11/2017.
 */
public class SquareImageView extends ImageView {
    private boolean square = true;
    private int width;
    private int height;

    public SquareImageView(Context context) {
        super(context);
    }

    public SquareImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (!square) {
            int w = MeasureSpec.getSize(widthMeasureSpec);
            double h = (double) height * ((double) w / (double) width);
            setMeasuredDimension(w, (int) h);
        }
        else super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    public void setSquare(boolean isSquare) {
        this.square = isSquare;
    }

    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }
}
