package com.alamkanak.imgur.presenter;

import com.alamkanak.imgur.model.ImgurImage;
import com.alamkanak.imgur.ui.ImgurMvpView;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

/**
 * Created by Alam on 4/11/2017.
 */

public class ImagePresenter extends MvpBasePresenter<ImgurMvpView<ImgurImage>> {
    public ImagePresenter() {

    }

    public void setImage(ImgurImage imgurImage) {
        getView().showData(imgurImage);
    }
}
