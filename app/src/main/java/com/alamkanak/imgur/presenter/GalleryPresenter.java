package com.alamkanak.imgur.presenter;


import android.app.Activity;
import android.text.TextUtils;

import com.alamkanak.imgur.ImgurSettings;
import com.alamkanak.imgur.R;
import com.alamkanak.imgur.api.ImgurApi;
import com.alamkanak.imgur.model.Gallery;
import com.alamkanak.imgur.model.ImgurImage;
import com.alamkanak.imgur.ui.ImgurMvpView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Alam on 4/10/2017.
 */
public class GalleryPresenter extends MvpBasePresenter<ImgurMvpView<List<ImgurImage>>> {

    public void fetchImages(boolean loadCache) {

        // Get cached response and display it in the UI.
        getView().showLoading();
        final ImgurSettings settings = new ImgurSettings((Activity) getView());
        if (loadCache) {
            String response = settings.getString(ImgurSettings.KEY_IMGUR_RESPONSE, "");
            if (!TextUtils.isEmpty(response)) {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<ImgurImage>>(){}.getType();
                List<ImgurImage> images = gson.fromJson(response, listType);
                getView().showData(images);
            }
        }

        // Fetch images from network.
        Call<Gallery> galleryCall = ImgurApi.getImgurApi().getGallery(
                settings.getString(ImgurSettings.KEY_SECTION, ((Activity) getView()).getString(R.string.section_hot)).toLowerCase(),
                settings.getString(ImgurSettings.KEY_SORT, ((Activity) getView()).getString(R.string.sort_viral)).toLowerCase(),
                settings.getString(ImgurSettings.KEY_WINDOW, ((Activity) getView()).getString(R.string.window_day)).toLowerCase(),
                0,
                settings.getBoolean(ImgurSettings.KEY_SHOW_VIRAL, true)
        );
        galleryCall.enqueue(new Callback<Gallery>() {
            @Override
            public void onResponse(Call<Gallery> call, Response<Gallery> response) {
                List<ImgurImage> images = response.body().getImages();
                getView().showData(images);
                settings.write(ImgurSettings.KEY_IMGUR_RESPONSE, new Gson().toJson(images));
            }

            @Override
            public void onFailure(Call<Gallery> call, Throwable t) {
                getView().showError();
            }
        });
    }
}
