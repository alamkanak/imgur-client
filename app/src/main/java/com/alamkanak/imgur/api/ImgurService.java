package com.alamkanak.imgur.api;

import com.alamkanak.imgur.model.Gallery;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static android.R.attr.path;

/**
 * Created by Alam on 4/10/2017.
 */

public interface ImgurService {

    @GET("gallery/{section}/{sort}/{window}/{page}")
    Call<Gallery> getGallery(@Path("section") String section, @Path("sort") String sort, @Path("window") String window, @Path("page") int page, @Query("showViral") boolean showViral);

}
