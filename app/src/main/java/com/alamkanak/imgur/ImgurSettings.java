package com.alamkanak.imgur;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import static android.content.Context.MODE_PRIVATE;

/**
 * A class to store user settings.
 *
 * Created by Alam on 4/11/2017.
 */
public class ImgurSettings {


    @Retention(RetentionPolicy.SOURCE)
    @StringDef({
            KEY_SECTION,
            KEY_WINDOW,
            KEY_SORT,
            KEY_SHOW_VIRAL,
            KEY_STAGGERED,
            KEY_IMGUR_RESPONSE
    })
    public @interface PreferenceKey {}
    public static final String KEY_STAGGERED = "staggered";
    public static final String KEY_SECTION = "section";
    public static final String KEY_WINDOW = "window";
    public static final String KEY_SORT = "sort";
    public static final String KEY_SHOW_VIRAL = "viral";
    public static final String KEY_IMGUR_RESPONSE = "imgur_response";

    private final SharedPreferences mSharedPreferences;

    public ImgurSettings(Activity context) {
        mSharedPreferences = context.getPreferences(MODE_PRIVATE);
    }

    public String getString(@PreferenceKey String key, String defaultValue) {
        return mSharedPreferences.getString(key, defaultValue);
    }

    public boolean getBoolean(@PreferenceKey String key, boolean defaultValue) {
        return mSharedPreferences.getBoolean(key, defaultValue);
    }

    public void write(@PreferenceKey String key, String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void write(@PreferenceKey String key, boolean value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }
}
